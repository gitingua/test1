#!/usr/bin/env python2
# coding=utf-8
import sys, os

from urlparse import urlparse
from getpass import getpass
from base64 import b64encode, b64decode
from jinja2 import Environment, FileSystemLoader
import time

import httplib, urllib

from common import *
from handlers import *

SEC_PREF="type:"
if len(sys.argv) < 2:
    print "Usage: %s link_type" % sys.argv[0]
    sys.exit(0)

link_type = sys.argv[1]

if not config.has_section(SEC_PREF+link_type):
    print "Wrong type!" 
    sys.exit(1)
else:
    section = SEC_PREF+link_type

fields = config.get(section, "ask").split(",")
fields = [ f.strip() for f in fields ]

if link_type == 'mail' and len(sys.argv) < 4:
    print "Usage: %s mail <username> <user_email>" % sys.argv[0]
    sys.exit(0)

data = StoredData()
for f in fields:
    f_name = f
    ask_fun = raw_input
    if f.startswith("_"):
        f_name = f[1:]
        if sys.stdin.isatty():
            ask_fun = getpass
    if sys.stdout.isatty():
        prompt = "Enter %s: " % f_name
    else:
        prompt = ""
    if link_type == 'key':
        data[f_name] = b64encode(urllib.quote(ask_fun(prompt)))
    else:
        data[f_name] = ask_fun(prompt)

data['expire'] = int(time.time() + config.getint(section, "expire") * 3600)
data['views'] = config.getint(section, "views")
data['rcap'] = 'HN0HTcW3ZqZm5rxxHGwH7zgKmYOCOHCj0e7Qp3cv';

headers = {}
int_server = urlparse(config.get("net", "int_server"), 'http')
if int_server.netloc == '':
    link_host = int_server.path
else:
    link_host = int_server.netloc
if '@' in link_host:
    (web_login, link_host) = link_host.split("@")
    web_pwd = getpass("Enter your www password: ") 
    userAndPass = b64encode("%s:%s" % (web_login, web_pwd)).decode("ascii")
    headers.update({ 'Authorization' : 'Basic %s' %  userAndPass })
if int_server.scheme == 'https' or int_server.scheme == '':
    conn = httplib.HTTPSConnection(link_host)
elif int_server.scheme == 'http':
    conn = httplib.HTTPConnection(link_host)
else:
    raise ValueError("Wrong protocol: %s" % int_server.scheme)

if link_type == 'mail':
    try:
        conn.request("POST", "/encrypt/%s" % config.get(section, "type"), str(data), headers=headers)
        response = conn.getresponse()
        if response.status == 200:
            file_id = response.read()
            link = "https://%s/%s/%s" % (config.get("net", "ext_server"), config.get(section, "type"), file_id)
#            link = "http://%s/%s/%s" % (config.get("net", "ext_server"), config.get(section, "type"), file_id)
        else:
            raise ValueError("Bad response from server: %s %s" %(response.status, response.reason))
        env = Environment(loader=FileSystemLoader("."))
        template = env.get_template(config.get(section, "template"))
        msg = template.render({
           'username' 		: sys.argv[2].decode('utf-8'),
           'login'		: data['login'],
           'secret_link'	: link,
        })
        mail_send(sys.argv[2], sys.argv[3], config.get(section, "subject"), msg.encode('utf8'))
        print "The letter has sended to %s successful." % sys.argv[3]
        sys.exit(0)
    except Exception as e:
        sys.stderr.write("ERROR: %s" % e)
        sys.stderr.write("TRACE: %s" % GetException(e))
        raise ValueError()

conn.request("POST", "/encrypt/%s" % link_type, str(data), headers=headers)
response = conn.getresponse()
if response.status == 200:
    file_id = response.read()
    print "https://%s/%s/%s" % (config.get("net", "ext_server"), link_type, file_id)
#    print "http://%s/%s/%s" % (config.get("net", "ext_server"), link_type, file_id)
else:
    print "Bad response from server: %s %s" %(response.status, response.reason) 
