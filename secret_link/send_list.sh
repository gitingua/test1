#!/bin/sh
#

while read str; do
	fname=$(echo ${str}|cut -d ';' -f 3)' '$(echo ${str}|cut -d ';' -f 2)
	email=$(echo ${str}|cut -d ';' -f 9)
	login=$(echo ${str}|cut -d ';' -f 6)
	password=$(echo ${str}|cut -d ';' -f 7)
	if [ -z "${email}" ]; then
		echo "email not found for user ${login}"
		continue
	fi
	echo "try sending mail for ${fname} to ${email}"
	echo "${login}\n${password}\n" | ./make_link.py mail "${fname}" "${email}"
	#sleep 20
done
