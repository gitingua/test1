import os
import os.path
import re

from handlers import *
from common import *
from getpass import getpass

if g.key is None:
    g.set_key(getpass("Enter the key: "))
#    g.set_key(encode("eHohtij0Thi2ohc7asaach4Thoh7ohF4", 'hex'))

def cleaner(environ, start_response):
    to_unlink = []
    def walker(arg, dirname, files):
        filenames = [ os.path.join(dirname, f) for f in files ]
        for f in filenames:
            print "Checking %s" % f
            try:
                data = StoredData()
                data.from_file(f)
                if data.expired() or int(data['views']) <= 0:
                    to_unlink.append(f)

            except NotFound:
                None

            except BadInput:
                # TODO: Logging of badly formed files
                to_unlink.append(f)

    os.path.walk(g.cache_dir, walker, None)
    [ os.unlink(f) for f in to_unlink ]
    response_headers = [("Content-type", "text/plain")]

    response = [ "OK" ]
    start_response("200 OK", response_headers)
    return response

def validation(environ, start_response):
    response = ['none']
    if RR.askRating(environ['REMOTE_ADDR']) >= config.getint('reCaptcha', 'count'):
        response = [ 'reCaptcha' ]

    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]
    start_response("200 OK", response_headers)
    return response

def getRCap(environ):
    if RR.askRating(environ['REMOTE_ADDR']) >= config.getint('reCaptcha', 'count'):
        RCap = {
            'host' : config.get('reCaptcha', 'host'),
            'url' : config.get('reCaptcha', 'url'),
            'secret' : config.get('reCaptcha', 'secret')
        }
        if config.has_option('reCaptcha', 'proxy'):
            RCap.update({'proxy':config.get('reCaptcha', 'proxy')})
    else:
        RCap = None
    RR.incRating(environ['REMOTE_ADDR'])
    return RCap

def encryptor(environ, start_response, data):

    rnd_id = create_encrypted(data, environ['onetime_id'])

    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]

    response = [ rnd_id ]
    start_response("200 OK", response_headers)
    return response

def checkRCap(data, rcap):
    if 'rcap' not in data:
        raise reCaptcha
    if data['rcap'] != 'HN0HTcW3ZqZm5rxxHGwH7zgKmYOCOHCj0e7Qp3cv':
        import httplib, json
        if 'proxy' in rcap:
            rcap['url'] = 'https://%s%s' % (rcap['host'], rcap['url'])
            host = rcap['proxy']
        else:
            host = rcap['host']
        host = host.split(':')
        if host.__len__() > 1:
            port = host[1]
        else:
            port = 443
        host = host[0]
        if port == 443:
            conn = httplib.HTTPSConnection(host, port, timeout=5)
        else:
            conn = httplib.HTTPConnection(host, port, timeout=5)
#           conn.set_debuglevel(10)
        conn.request('POST', rcap['url'], 'secret=' + rcap['secret'] + '&response=' + str(data['rcap']), {'Content-Type':'application/x-www-form-urlencoded'})
        try:
            response = conn.getresponse()
            if response.status != 200 or not json.loads(response.read())['success']:
                raise reCaptcha
        except:
            raise reCaptcha

def decryptor(environ, start_response):
    response = []
    body = ''
    try:
        (url, file_id) = environ['PATH_INFO'].strip('/').split('/')
        environ['onetime_id'] = file_id
        environ['onetime_url'] = url
        if url == 'clean':
            return cleaner(environ, start_response)
        if url == 'validation':
            return validation(environ, start_response)
        if url == 'qrcode':
            url = 'show_qrcode'
        try:
            length = int(environ.get('CONTENT_LENGTH', '0'))
        except ValueError:
            length = 0
        if length > 1024 * 64:
            raise BadLength
        if length != 0:
            body = environ['wsgi.input'].read(length)
        RCap = getRCap(environ)


        if url == 'encrypt' and file_id in URLS:
            if RCap and length == 0:
                raise reCaptcha
            data = StoredData()
            data.from_string(body)
            if RCap:
                checkRCap(data, RCap)
            return encryptor(environ, start_response, data)

        if RCap:
            try:
                if length == 0:
                    raise reCaptcha
                data = StoredData()
                data.from_string(body)
                checkRCap(data, RCap)
            except reCaptcha:
                return getRCap_handler(environ, start_response)

        if url[:5] != 'show_':
            return show_handler(environ, start_response)

        url = url[5:]

        if url in URLS and re.match('^[a-fA-F0-9]+$', file_id):
            data = StoredData()
            filename = os.path.join(g.cache_dir, url, file_id)
            data.from_file(filename)
            if data.expired():
                raise Expired
            data.view(filename)
            environ['onetime_query'] = data
            return URLS[url](environ, start_response)
        else:
            raise BadInput
    except Expired:
        return expire_handler(environ, start_response)

    except NotFound:
        return not_found_handler(environ, start_response)

    except reCaptcha:
        return reCaptcha_handler(environ, start_response)

    except BadLength:
        return bad_length_handler(environ, start_response)

    except LimitExceeded:
        return bad_limit_handler(environ, start_response)

    except Exception as e:
        print GetException(e)
        return bad_input_handler(environ, start_response)
