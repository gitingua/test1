import os.path
import smtplib
import threading

from codecs import encode, decode
from base64 import b64encode
from time import time as now
from ConfigParser import SafeConfigParser

from Crypto.Cipher import AES
from Crypto import Random

lock = threading.Lock()

# Some global storage
class Global:
    def __init__(self):
        self.key = None
        root =  str(os.path.curdir)
        self.cache_dir = root+"/cache"
        self.expire = 300
        self.view_limit = 5
        #336 = 14 days
        self.max_expire = 336
        self.max_view = 10

    def set_key(self, key):
        self.key = decode(key, 'hex')

g = Global()

class Expired(Exception):
    None

class NotFound(Exception):
    None

class BadInput(Exception):
    None

class reCaptcha(Exception):
    None

class BadLength(Exception):
    None

class LimitExceeded(Exception):
    None

class ReqRating:
    def __init__(self, config):
        self.Rating = {}
        self.config = config
    def askRating(self,ip):
        self.clean()
        if ip not in self.Rating:
            return 0
        else:
            return self.Rating[ip].__len__()
    def incRating(self,ip):
        if ip not in self.Rating:
            self.Rating[ip] = []
        self.Rating[ip].append(now())
    def clean(self):
        global lock
        lock.acquire()
        old = now() - self.config.getint('reCaptcha', 'delta')
        for ip in self.Rating.keys():
            self.Rating[ip] = filter((lambda x: x > old), self.Rating[ip])
            if self.Rating[ip].__len__() == 0:
                self.Rating.__delitem__(ip)
        lock.release()

class StoredData(dict):
    def __init__(self, *args, **kwargs):
        super(StoredData, self).__init__(*args, **kwargs)

    def from_ciphertext(self, ciphertext):
        self.from_string(decrypt(ciphertext))

    def from_string(self, instr):
        qs = instr.split('\0')
        if len(qs) % 2 != 0:
            raise BadInput
        query = dict(zip(qs[::2], qs[1::2]))
        self.update(query)
        if not 'expire' in self:
            self['expire'] = 0
        if not 'views' in self:
            self['views'] = 0
        self['views'] = int(self['views'])
        self['expire'] = int(self['expire'])

    def from_file(self, filename):
        if not os.path.isfile(filename):
            raise NotFound
        infile = file(filename, 'rb')

        try:
            str_ = decrypt(infile.read(os.path.getsize(filename)))
        except:
            raise BadInput
        infile.close()
        self.from_string(str_)

    def to_file(self, filename):
        outfile = file(filename, 'wb')
        outfile.write(encrypt(str(self)))
        outfile.close()

    def to_random_file(self, path):
        while True:
            rnd_id = encode(Random.new().read(32), 'hex')
            filename = os.path.join(path, rnd_id)
            if not os.path.isfile(filename):
                break
        self.to_file(filename)
        return rnd_id

    def view(self, filename):
        if self['views'] <= 0:
            if os.path.isfile(filename):
                os.unlink(filename)
            raise NotFound
        self['views'] -= 1
        self.to_file(filename)

    def expired(self):
        return self['expire'] < now()

    def add_expiration(self, expiration):
        expire = int(now() + expiration)
        self['expire'] = expire

    def limit_views(self, views):
        self['views'] = views

    def __str__(self):
        return '\0'.join(
            [ str(k)+'\0'+str(v) for (k,v) in self.items() ]
        )

    def encrypt(self):
        return encrypt(str(self))

def GetException(e):
    import sys, traceback
    tb = traceback.extract_tb(sys.exc_info()[2], 1)[0]
    if e.message != '':
        msg = e.message
    elif e.args.__len__() > 1:
        msg = e.args[1]
    else:
        msg = e
        for t in traceback.extract_tb(sys.exc_info()[2]):
            print t
    return 'File "%s" at line %d has an exception <%s>: %s' % (tb[0], tb[1], type(e).__name__, msg)

def create_encrypted(data, subdir, rnd_id=None):
    if not 'expire' in data:
        data.add_expiration(g.expire)

    if not 'views' in data:
        data.limit_views(g.view_limit)
    filepath = os.path.join(g.cache_dir, subdir)

    if config.has_option('type:%s' % subdir, 'max_expire'):
        max_expire = config.getint('type:%s' % subdir, 'max_expire')
    else:
        max_expire = g.max_expire
    if config.has_option('type:%s' % subdir, 'max_view'):
        max_view = config.getint('type:%s' % subdir, 'max_view')
    else:
        max_view = g.max_view

    if data['expire'] < now() or data['expire'] > int(now() + max_expire * 3600):
        raise LimitExceeded
    if data['views'] > max_view:
        raise LimitExceeded

    if not os.path.exists(filepath):
        os.makedirs(filepath, mode=0755)
    if rnd_id == None:
        rnd_id = data.to_random_file(filepath)
    else:
        data.to_file(os.path.join(filepath, rnd_id))
    return rnd_id

def encrypt(message):
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(g.key, AES.MODE_CFB, iv)
    return encode(iv + cipher.encrypt(message), 'hex')

def decrypt(hex_ciphertext):
    key = g.key
    enc_bytes = decode(hex_ciphertext, 'hex')
    iv = enc_bytes[:AES.block_size]
    ciphertext = enc_bytes[AES.block_size:]
    cipher = AES.new(key, AES.MODE_CFB, iv)
    return cipher.decrypt(ciphertext)

def mail_send(rcpt_name, rcpt_mail, subj, msg):
    mh = "Content-Transfer-Encoding: base64\nContent-type: text/html; charset=utf-8\nMIME-Version: 1.0\n"
    mh += "From: %s\n" % (config.get("smtp", "sender"))
    mh += "To: %s <%s>\n" % (rcpt_name, rcpt_mail)
    mh += "Subject: =?utf-8?B?%s?=\n\n" % (b64encode(subj))
    mb = mh + b64encode(msg)
    s = smtplib.SMTP(config.get("smtp", "server"), config.getint("smtp", "port"))
    s.starttls()
    s.login(config.get("smtp", "login"), config.get("smtp", "password"))
    s.sendmail(config.get("smtp", "sender"), rcpt_mail, mb)

config_path = "./config.ini"
config = SafeConfigParser()
config.read(config_path)
RR = ReqRating(config)
