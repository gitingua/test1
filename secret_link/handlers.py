import sys
from StringIO import StringIO
from datetime import datetime as dt

from jinja2 import Environment, FileSystemLoader
import qrcode
import pyotp
from common import *

URLS= {
}

def vpn_handler(environ, start_response):
    query = environ['onetime_query']
    # Check expiration
    try:
        # Check if all keys exist
        for k in ['pin', 'key', 'user']:
            if not k in query:
                raise BadInput
        env = Environment(loader=FileSystemLoader("templates/"))
        template = env.get_template("vpn.tmpl")
        qrcode_str = StoredData()
        qrcode_str['url'] = pyotp.totp.TOTP(query['key']).provisioning_uri('%s@vpn.rambler-co.ru' % query['user'])
        qrcode_str['expire'] = query['expire']
        qrcode_str.limit_views(query['views']+1)
        filepath =  os.path.join(g.cache_dir, 'qrcode')
        if not os.path.exists(filepath):
            os.makedirs(filepath, mode=0755)

        qrcode_str.to_file(os.path.join(filepath, environ['onetime_id']))

        response_headers = [("Content-type", "text/html"), ('Content-encoding', 'UTF-8')]
        start_response("200 OK", response_headers)
        return  [encode(template.render(
            qrcode_str=environ['onetime_id'],
            expire_iso=dt.fromtimestamp(float(qrcode_str['expire'])).isoformat()
        , **query), 'utf8')]

   # except ZeroDivisionError as e:
    except Exception as e:
        sys.stderr.write("ERROR: %s" % e)
        raise ValueError()

def qrcode_handler(environ, start_response):
    query = environ['onetime_query']
    qr = qrcode.QRCode(box_size=6)
    qr.add_data(query['url'])
    img = qr.make_image()
    s = StringIO()
    img.save(s)
    response_headers = [("Content-type", "image/png")]
    start_response("200 OK", response_headers)
    return  [s.getvalue()]

def getRCap_handler(environ, start_response):
    env = Environment(loader=FileSystemLoader("templates/"))
    template = env.get_template("recaptcha.tmpl")
    response_headers = [("Content-type", "text/html")]
    start_response("200 OK", response_headers)
    if environ['onetime_url'][:5] != 'show_':
        environ['onetime_url'] = 'show_' + environ['onetime_url']
    return  [encode(template.render(
        path = environ['PATH_INFO'],
        id = environ['onetime_id'],
        url = environ['onetime_url']
        ), 'utf8')]

def show_handler(environ, start_response):
    env = Environment(loader=FileSystemLoader("templates/"))
    template = env.get_template("show.tmpl")
    response_headers = [("Content-type", "text/html")]
    start_response("200 OK", response_headers)
    return  [encode(template.render(
        path = environ['PATH_INFO'],
        id = environ['onetime_id'],
        url = 'show_' + environ['onetime_url']
        ), 'utf8')]

def expire_handler(environ, start_response):
    env = Environment(loader=FileSystemLoader("templates/"))
    template = env.get_template("expire.tmpl")
    response_headers = [("Content-type", "text/html")]
    start_response("200 OK", response_headers)
    return  [str(template.render())]

def not_found_handler(environ, start_response):
    response_headers = [("Content-type", "text/plain")]
    response = ["Not found"]
    start_response("404 Not found", response_headers)
    return response

def bad_input_handler(environ, start_response):
    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]
    response = ["Bad input"]
    start_response("400 Bad input", response_headers)
    return response

def reCaptcha_handler(environ, start_response):
    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]
    response = ["reCaptcha"]
    start_response("400 reCaptcha", response_headers)
    return response

def bad_length_handler(environ, start_response):
    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]
    response = ["Bad length"]
    start_response("400 BadLength", response_headers)
    return response

def bad_limit_handler(environ, start_response):
    response_headers = [("Content-type", "text/plain"),("Access-Control-Allow-Origin", "*")]
    response = ["Limit is exceeded"]
    start_response("400 LimitExceeded", response_headers)
    return response

def key_handler(environ, start_response):
    query = environ['onetime_query']
    try:
        # Check if all keys exist
        if not 'key' in query:
            raise BadInput

        response_headers = [("Content-type", "text/html"), ('Content-encoding', 'UTF-8')]
        start_response("200 OK", response_headers)
        env = Environment(loader=FileSystemLoader("templates/"))
        template = env.get_template("key.tmpl")
        import base64
        query['key'] = base64.b64decode(query['key'])
        return  [encode(template.render(
            expire_iso=dt.fromtimestamp(float(query['expire'])).isoformat()
        , **query), 'utf8')]

   # except ZeroDivisionError as e:
    except Exception as e:
        sys.stderr.write("ERROR: %s" % e)
        raise ValueError()

def password_handler(environ, start_response):
    query = environ['onetime_query']
    try:
        # Check if all keys exist
        for k in ['login', 'password']:
            if not k in query:
                raise BadInput
        env = Environment(loader=FileSystemLoader("templates/"))
        template = env.get_template("password.tmpl")
        if 'template' in query and query['template'] != '':
            template = env.get_template(query['template'] + '.tmpl')
        response_headers = [("Content-type", "text/html"), ('Content-encoding', 'UTF-8')]
        start_response("200 OK", response_headers)
        return  [encode(template.render(
            expire_iso=dt.fromtimestamp(float(query['expire'])).isoformat()
        , **query), 'utf8')]

   # except ZeroDivisionError as e:
    except Exception as e:
        sys.stderr.write("ERROR: %s" % e)
        raise ValueError()

URLS['vpn'] = vpn_handler
URLS['qrcode'] = qrcode_handler
URLS['password'] = password_handler
URLS['key'] = key_handler
