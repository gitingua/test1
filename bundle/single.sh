#!/bin/sh
#

if [ -z "$1" ]; then
	echo "Usage: $0 <FQDN>"
	exit
fi

DOM=`echo $1 | sed -n 's/^\|\(.*\.\)\([^.]\{2,\}\.[^.]*\)$/\2/p'`
ORG=`whois "${DOM}" | grep org | sed -n 's/^org: *\(.*\)/\1/p' | tr -d '"' | tr ',' ' '`
if [ -z "${ORG}" ]; then
	ORG=`whois "${DOM}" | grep 'Registrant Organization' | sed -n 's/^Registrant Organization: *\(.*\)/\1/p' | tr -d '"' | tr ',' ' '`
fi
base=`echo $1 | sed -e 's/*\./wc_/'`
self=`basename $0`

if [ "${self}" = "single_ecc.sh" ]; then
	openssl ecparam -out private/"${base}".key -name prime256v1 -genkey
else
	openssl genrsa -out private/"${base}".key 2048
fi

if [ $? -ne 0 ]; then
	echo 'Creating the key has an error'
	exit 1
fi
pwgen -s -1 14 > private/"${base}".p12.pass

export ORG
######export OPENSSL_CONF=single.cnf
#CN=$1 openssl req -new -config single.cnf -key private/"${base}".key -out "${base}".csr
CN=$1 openssl req -new -config common.cnf -key private/"${base}".key -out "${base}".csr
if [ $? -ne 0 ]; then
	echo 'Creating the request has an error'
	exit 1
fi
SUBJ=`openssl req -subject -noout -in "${base}".csr`
CSR=`openssl req -in "${base}".csr`
echo "=====================\n"
echo "${SUBJ}\n\n{noformat}\n${CSR}\n{noformat}"
echo "\n====================="

read -p 'SSL-task number: ' task
git add .
git commit -m "SSL-${task}"
git push