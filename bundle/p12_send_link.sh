#!/bin/sh
#

for param in email file subj; do
	if [ -n "$1" ]; then
		eval "${param}=\"$1\""
	fi
	if [ $# -gt 1 ]; then
		shift
	else
		break
	fi
done

if [ -z "${email}" ]; then
	read -p 'Send the secret to: ' email
fi
if [ -z "${file}" ]; then
	read -p 'The secret of the base file: ' file
fi
if [ -z "${subj}" ]; then
	read -p 'SSL-task number: ' task
	if [ -n "${task}" ]; then
		subj="JIRA (${task})"
	fi
fi

if [ -z "${subj}" ]; then
	subj="For domain: ${file}"
fi

subj=`echo -n "${subj}" | base64 -w 0`

my_home=`dirname $0`
if [ "${my_home}" = "." ]; then
	my_home=$(pwd)
fi

if [ ! -s "${my_home}/private/${file}.p12.pass" ]; then
	echo "File with p12.pass not found";
	exit
fi 

cd ../secret_link
slink=`echo "${file}.p12\n$(cat ${my_home}/private/${file}.p12.pass)\n\n" | ./make_link.py password`

echo "${slink}"
#exit

mbody=`cat - | base64` <<END
Привет,

Это тебе потребуется:

${slink}

ВНИМАНИЕ! Эта ссылка будет доступна только в течении 72 часа с момента получения, после чего информация станет недоступна.

END

ssmtp -au${USER} -ap`pass personal/RamblerCO/MicrosoftAD/mereschenko` ${email} <<END
Content-Transfer-Encoding: base64
Content-type: text/plain; charset=utf-8
Subject: =?utf-8?B?${subj}?=
MIME-Version: 1.0
From: "Mikhail Ereschenko" <MEreschenko@rambler-co.ru>
To: ${email}

${mbody}
END

if [ $? -eq 0 ]; then
	echo "done"
else
	echo "have an error while mail is sending, you must send an email by yourself"
	echo "==============="
	echo "Subject: $(echo "${subj}" | base64 -d)"
	echo "---"
	echo "${mbody}" | base64 -d
fi

cd "${my_home}"
