#!/bin/bash
#

if [ -z "$1" ]; then
	echo "Usage: $0 <FQDN>"
	exit
fi

san_list=`cat san.list | sort -k 1 | uniq`
export SAN=`echo "${san_list}" | xargs -I {} echo -n DNS:{}, | sed -e 's/,$//'`
#export SAN
#export OPENSSL_CONF=common_san.cnf

file="san_$1"

#DOM=`echo $1 | sed -n 's/^\|\(.*\.\)\([^.]\{2,\}\.[^.]*\)$/\2/p'`
#ORG=`whois "${DOM}" | grep org | sed -n 's/^org: *\(.*\)/\1/p' | tr -d '"' | tr ',' ' '`
export ORG='Rambler Internet Holdings LLC'

openssl genrsa -out private/"${file}".key 2048
pwgen -s -1 14 > private/"${file}".p12.pass

#export ORG
######export OPENSSL_CONF=single.cnf
CN="$1" openssl req -new -config common_san_auto.cnf -key private/"${file}".key -out "${file}".csr
SUBJ=`openssl req -subject -noout -in "${file}".csr`
SAN_EXT=`openssl req -text -noout -in "${file}".csr | grep 'X509v3 Subject Alternative Name' -A 1 | tr -s ' ' | sed 's/^ //'`
CSR=`openssl req -in "${file}".csr`
echo "=====================\n"
echo "${SUBJ}\n\n${SAN_EXT}\n\n{noformat}\n${CSR}\n{noformat}"
echo "\n====================="

