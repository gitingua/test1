#!/bin/sh
#

my_home=`dirname $0`
if [ "${my_home}" = "." ]; then
	my_home=`pwd`
fi

if [ -z "$1" ]; then
	read -p 'base file: ' file
else
	file=$1
fi

if [ -z "$2" ]; then
	read -p 'crt file: ' crt
else
	crt=$2
fi

if [ -z "$3" ]; then
	read -p 'intermediate file: ' inter
else
	inter=$3
fi

cd "${my_home}"

openssl pkcs12 -export -in "${crt}" -certfile "${inter}" -out "${file}.p12" -inkey private/"${file}".key -password file:private/"${file}".p12.pass

if [ $? -ne 0 ]; then
	echo 'Creating PKCS12 has an error'
	exit 1
fi



read -p 'Send the secret to: ' email
if [ -z "${email}" ]; then
	echo "Nothing to send"
	exit
fi
read -p 'SSL-task number: ' task

git add .
git commit -m "SSL-${task}"
git push

echo ${USER}
cd ../secret_link
slink=`echo "${file}.p12\n$(cat ${my_home}/private/${file}.p12.pass)\n\n" | ./make_link.py password`
echo ${slink}
cd "${my_home}"


if !( echo "${slink}" | grep -E '^https://secret' > /dev/null); then
        echo "creating secret link error"
        echo "slink=${slink}"
        exit
fi


mbody=`echo "\nПривет, \nЭто тебе потребуется: \n${slink} \nВНИМАНИЕ! Эта ссылка будет доступна только в течении 72 часа с момента получения, после чего информация станет недоступна." | base64`

if [ -n "${task}" ]; then
	subj="JIRA (${task})"
else
	subj="For domain: ${file}"
fi

subj=`echo "${subj}" | base64`

sendmail -v ${email} <<END
Content-Transfer-Encoding: base64
Content-type: text/plain; charset=utf-8
Subject: =?utf-8?B?${subj}?=
MIME-Version: 1.0
From: "Gitinomagomed Magomedov" <gi.magomedov@rambler-co.ru>
To: ${email}

${mbody}
END

echo ${USER}
if [ $? -eq 0 ]; then
	echo "done"
else
	echo "have an error while mail is sending, you must send an email by yourself"
	echo "==============="
	echo "Subject: ${subj}"
	echo "---"
	echo ${mbody} | base64 -d
fi

cd "${my_home}"
#####################
