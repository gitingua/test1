#!/bin/sh
#

for param in email file subj; do
	if [ -n "$1" ]; then
		eval "${param}=\"$1\""
	fi
	if [ $# -gt 1 ]; then
		shift
	else
		break
	fi
done

if [ -z "${email}" ]; then
	read -p 'Send the secret to: ' email
fi
if [ -z "${file}" ]; then
	read -p 'The secret of the base file: ' file
fi
if [ -z "${subj}" ]; then
	read -p 'SSL-task number: ' task
	if [ -n "${task}" ]; then
		subj="JIRA (${task})"
	fi
fi

if [ -z "${subj}" ]; then
	subj="For domain: ${file}"
fi

subj=`echo -n "${subj}" | base64 -w 0`

my_home=`dirname $0`
if [ "${my_home}" = "." ]; then
	my_home=$(pwd)
fi

if [ ! -s "${my_home}/private/${file}.p12.pass" ]; then
	echo "File with p12.pass not found"
	exit
fi 

if [ ! -s "${my_home}/${file}.p12" ]; then
	echo "File with p12 not found"
	exit
fi

P12=$(cat "${my_home}/${file}.p12" | base64)

if [ -z "${P12}" ]; then
	echo "encoding P12 error"
	exit
fi

cd ../secret_link
slink=`echo "${file}.p12\n$(cat ${my_home}/private/${file}.p12.pass)\n\n" | ./make_link.py password`
cd "${my_home}"

if !( echo "${slink}" | grep -E '^https://secret' > /dev/null); then
	echo "creating secret link error"
	echo "slink=${slink}"
	exit
fi

boundary=$(dd if=/dev/urandom ibs=100 count=10 2>/dev/null | tr -cd '[:alnum:]_+-' | sed -ne 's/^\(.\{30\}\).*$/\1/p')
boundary="--==_mimepart_${boundary}"

if [ -z "${boundary}" ]; then
	echo "creating boundary error"
	exit
fi


message=`cat - | base64` <<END
Привет,

PKCS12 со всеми нужными сертификатами и ключом в аттаче. Чтобы открыть контейнер воспользуйся ссылкой:

${slink}

ВНИМАНИЕ! Эта ссылка будет доступна только в течении 72 часа с момента получения, после чего информация станет недоступна.

END

mbody=$(cat -) <<END
--${boundary}
Content-Transfer-Encoding: base64
Content-Type: text/plain; charset=UTF-8

${message}

--${boundary}
Content-Type: application/pkcs12;filename=${file}.p12
Content-Transfer-Encoding: base64
Content-Disposition: attachment;filename="${file}.p12"

${P12}

--${boundary}--
END

ssmtp -au${USER} -ap`pass personal/RamblerCO/MicrosoftAD/mereschenko` ${email} <<END
MIME-Version: 1.0
From: "Mikhail Ereschenko" <MEreschenko@rambler-co.ru>
To: ${email}
Subject: =?utf-8?B?${subj}?=
Content-Type: multipart/mixed; boundary="${boundary}"; charset=UTF-8
Content-Transfer-Encoding: 7bit

${mbody}
END

if [ $? -eq 0 ]; then
	echo "done"
else
	echo "have an error while mail is sending, you must send an email by yourself"
	echo "==============="
	echo "Subject: $(echo "${subj}" | base64 -d)"
	echo "---"
	echo "${message}" | base64 -d
fi
