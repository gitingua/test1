#!/bin/bash

SAN=`cat san.list | sort -k 1 | uniq | xargs -I {} echo -n DNS:{}, | sed -e 's/,$//'`
export SAN
export OPENSSL_CONF=common_san.cnf
openssl $@
